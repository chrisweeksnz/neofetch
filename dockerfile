FROM alpine:latest
LABEL maintainer="Chris Weeks <chris@weeks.net.nz>" \
      project-site="https://github.com/dylanaraps/neofetch" \
      container-site="https://gitlab.com/chrisweeksnz/neofetch"
RUN apk update && \
    apk add bash curl && \
    curl -Lo /neofetch.zip https://github.com/dylanaraps/neofetch/archive/master.zip && \
    unzip /neofetch.zip && \
    /neofetch-master/neofetch --print_config > /neofetch-master/config && \
    sed -i \
        -e 's/# info "Local IP" local_ip/info "Local IP" local_ip/' \
        -e 's/# info "Public IP" public_ip/info "Public IP" public_ip/' \
        /neofetch-master/config
ENTRYPOINT ["/neofetch-master/neofetch", "--config", "/neofetch-master/config"]
