# Neofetch thanks...

This docker container is a simple neofetch container.

It is a direct implementation on alpine of neofetch.
- https://github.com/dylanaraps/neofetch

# How to use

Basic use:
    
```shell
docker run \
    -it --rm \
    --hostname=${HOSTNAME} \
    --net=host \
    -v /etc/os-release:/etc/os-release:ro \
    registry.gitlab.com/chrisweeksnz/neofetch
```

I prefer to add the following to ~/.bashrc:

```shell
function neofetch ()
{
	echo
	docker run \
            -it --rm \
            --runtime=runc \
            --hostname=${HOSTNAME} \
            --net=host \
            -v /etc/os-release:/etc/os-release:ro \
            registry.gitlab.com/chrisweeksnz/neofetch $@
	echo -e '\n'
}
neofetch
```

To replace the configuration file, overlay ```/neofetch-master/config```:

```shell
# Get a copy of the configuration file
docker run --runtime=runc -it --rm --entrypoint sh registry.gitlab.com/chrisweeksnz/neofetch -c 'cat /neofetch-master/config' > neofetch-config

# Edit the configuration file to suit your needs
vi neofetch-config

# Overlay the config at runtime (change your function if using that method)
docker run \
    -it --rm \
    --hostname=${HOSTNAME} \
    --net=host \
    -v /etc/os-release:/etc/os-release:ro \
    -v ${PWD}/neofetch-config:/neofetch-master/config:ro \
    registry.gitlab.com/chrisweeksnz/neofetch
```

When developing neofetch, overlay ```/neofetch-master/neofetch``` and test using 
arguments. The following example uses Clear Linux: 

```shell
docker run \
    -it --rm \
    -v ${PWD}/neofetch:/neofetch-master/neofetch \
    registry.gitlab.com/chrisweeksnz/neofetch --ascii_distro 'Clear Linux OS'
```