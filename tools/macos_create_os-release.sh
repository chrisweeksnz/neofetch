#!/bin/bash

# This script generates a os-release file that is syntactically correct (https://www.freedesktop.org/software/systemd/man/os-release.html)

# This can be used with the likes of neofetch afterwards

# The script must be run as root and should be executable

echo -e "NAME=\"$(sw_vers -productName)\"\nVERSION=\"$(sw_vers -productVersion) ($(sw_vers -buildVersion))\"\nID=$(sw_vers -productName | sed 's/ //g' | tr A-Z a-z)\nVERSION_ID=$(sw_vers -productVersion)\nPRETTY_NAME=\"$(sw_vers -productName) $(sw_vers -productVersion) ($(sw_vers -buildVersion))\"\nHOME_URL=\"https://www.apple.com/macos/\"\nBUG_REPORT_URL=\"https://developer.apple.com/bug-reporting/\"" > /etc/os-release